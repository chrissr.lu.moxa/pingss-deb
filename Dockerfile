FROM debian:buster

# Install Dependencies
RUN echo "deb-src http://deb.debian.org/debian buster-updates main" >> /etc/apt/sources.list
RUN apt-get update -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    apt-utils \
    build-essential \
    git \
    ca-certificates \
    devscripts \
    fakeroot \
    quilt \
    debhelper \
    dh-make \
    vim

# quilte 3.0
RUN echo "alias dquilt=quilt --quiltrc=${HOME}/.quiltrc-dpkg" >> ~/.bashrc
RUN echo "complete -F _quilt_completion -o filenames dquilt" >> ~/.bashrc
RUN printf '\
d=. ; while [ ! -d $d/debian -a $(readlink -e $d) != / ]; do d=$d/..; done \n\
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then \n\
    QUILT_PATCHES="debian/patches" \n\
    QUILT_PATCH_OPTS="--reject-format=unified" \n\
    QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto" \n\
    QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index" \n\
    QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:diff_ctx=35:diff_cctx=33" \n\
    if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi \n\
fi\n' >> ~/.quiltrc-dpkg


# Environment and Directory
RUN mkdir -p /debian/pingss-1.0
ENV DEBEMAIL="ChrisSR_Lu@moxa.com"
ENV DEBFULLNAME="Sin-Ru Lu"
WORKDIR /debian/pingss-1.0
