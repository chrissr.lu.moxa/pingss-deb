#!/bin/sh
DIR=$1
PROJECTHOME=${DIR##*/}
PROJECTNAME=$(echo $PROJECTHOME | cut -d '-' -f1)

cp src/systemd/pingss.service $DIR/debian

echo $PROJECTNAME
echo "src/bin/pingss /usr/bin" >> $DIR/debian/$PROJECTNAME.install
